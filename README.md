# Crafty Controller
> Python based Server Manager / Web Portal for your Minecraft Server

### This is a fork of Crafty Controller intended to be used for docker only

> This image now supports 1.17!

## What is Crafty?
Crafty is a Minecraft Server Wrapper / Controller / Launcher. The purpose 
of Crafty is to launch a Minecraft server in the background and present 
a web interface for the admin to use to interact with their server. Crafty 
is compatible with docker on Windows (7, 8, 10) and Linux. 

## Features
- [Tornado](https://www.tornadoweb.org/en/stable/) webserver used as a backend for the web side.
- [Argon2](https://pypi.org/project/argon2-cffi/) used for password hashing
- [SQLite DB](https://www.sqlite.org/index.html) used for settings.
- [Adminlte](https://adminlte.io/themes/AdminLTE/index2.html) used for web templating
- [Font Awesome 5](https://fontawesome.com/) used for Buttons 

## How does it work?
Crafty is launched via docker. 
Crafty will then automatically start a Tornado web server on the back end, 
as well as your Minecraft server if auto-start is enabled. You can remotely 
manage your server via the web interface, either on a PC, or on your phone. 
Logins are secure and use the most advanced web security models available.

## Supported OS?
- Linux - specifically Ubuntu and others if they run docker
- Windows (7,8,10) via docker
- OS X (although untested)

## Installation
```
docker run \
    -v /path/to/host/servers:/minecraft_servers \
    -v /path/to/host/db:/crafty_db \
    -p 8000:8000 \
    -p 25500-25600 \
    -d \
zanda8893/crafty:latest
```

## Documentation
Check out our shiny new documentation [right on GitLab](https://gitlab.com/crafty-controller/crafty-web/wikis/home).


[GIT Repo](https://gitlab.com/zanda8893/crafty-web)
