### 1. Get Linux
FROM alpine:latest

### 2. Label
LABEL maintainer="Dockerfile created by zanda8893 <https://gitlab.com/zanda8893>"

### 3. Get Java 17
RUN apk update \
&& apk upgrade \
&& apk add --no-cache bash \
&& wget https://download.java.net/java/early_access/alpine/14/binaries/openjdk-17-ea+14_linux-x64-musl_bin.tar.gz \
&& tar -zxf openjdk-17-ea+14_linux-x64-musl_bin.tar.gz \ 
&& rm openjdk-17-ea+14_linux-x64-musl_bin.tar.gz

### 4. Add Java to PATH
ENV PATH "$PATH:/jdk-17/bin"

### 5. Copy requirements.txt

COPY ./requirements.txt /crafty_web/

### 6. Get build tools, Python headers, python, pip, build modules, remove dependencies

RUN apk add --no-cache gcc \ 
&& apk add --no-cache python3-dev \
&& apk add --no-cache libffi-dev \
&& apk add --no-cache musl-dev \
&& apk add --no-cache openssl-dev \
&& apk add --no-cache python3 \
&& apk add --no-cache make \
&& python3 -m ensurepip \
&& pip3 install --upgrade pip setuptools wheel \
&& rm -r /usr/lib/python*/ensurepip && \
if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi \
&& pip3 install -r /crafty_web/requirements.txt \
&& apk del python3-dev musl-dev libffi-dev openssl-dev make \
&& rm -r /root/.cache 

### 7. Copy directory

COPY ./ /crafty_web

### 8. Expose web ui port
EXPOSE 8000

### 9. Expose mc server ports
EXPOSE 25500-25600

### 10. Set working directory
WORKDIR /crafty_web

### 11. Start crafty
CMD ["python3", "crafty.py", "-c", "/crafty_web/configs/docker_config.yml"]
